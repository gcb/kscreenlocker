# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kscreenlocker package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kscreenlocker\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-19 00:39+0000\n"
"PO-Revision-Date: 2024-04-19 13:45+0200\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.3.2\n"

#: ui/Appearance.qml:22
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "გარემოს იერსახე"

#: ui/Appearance.qml:38
#, kde-format
msgid "Wallpaper type:"
msgstr "ფონური სურათის ტიპი:"

#: ui/DurationPromptDialog.qml:114
#, kde-format
msgctxt "The unit of the time input field"
msgid "millisecond"
msgid_plural "milliseconds"
msgstr[0] "მილიწამი"
msgstr[1] "მილიწამი"

#: ui/DurationPromptDialog.qml:116
#, kde-format
msgctxt "The unit of the time input field"
msgid "second"
msgid_plural "seconds"
msgstr[0] "წამი"
msgstr[1] "წამი"

#: ui/DurationPromptDialog.qml:118
#, kde-format
msgctxt "The unit of the time input field"
msgid "minute"
msgid_plural "minutes"
msgstr[0] "წუთი"
msgstr[1] "წუთი"

#: ui/DurationPromptDialog.qml:120
#, kde-format
msgctxt "The unit of the time input field"
msgid "hour"
msgid_plural "hours"
msgstr[0] "საათი"
msgstr[1] "საათი"

#: ui/DurationPromptDialog.qml:122
#, kde-format
msgctxt "The unit of the time input field"
msgid "day"
msgid_plural "days"
msgstr[0] "დღე"
msgstr[1] "დღე"

#: ui/DurationPromptDialog.qml:124
#, kde-format
msgctxt "The unit of the time input field"
msgid "week"
msgid_plural "weeks"
msgstr[0] "კვირა"
msgstr[1] "კვირა"

#: ui/DurationPromptDialog.qml:126
#, kde-format
msgctxt "The unit of the time input field"
msgid "month"
msgid_plural "months"
msgstr[0] "თვე"
msgstr[1] "თვე"

#: ui/DurationPromptDialog.qml:128
#, kde-format
msgctxt "The unit of the time input field"
msgid "year"
msgid_plural "years"
msgstr[0] "წელი"
msgstr[1] "წელი"

#: ui/DurationPromptDialog.qml:155
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "milliseconds"
msgstr "მილიწამი"

#: ui/DurationPromptDialog.qml:157
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "seconds"
msgstr "წამი"

#: ui/DurationPromptDialog.qml:159
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "minutes"
msgstr "წუთი"

#: ui/DurationPromptDialog.qml:161
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "hours"
msgstr "საათი"

#: ui/DurationPromptDialog.qml:163
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "days"
msgstr "დღე"

#: ui/DurationPromptDialog.qml:165
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "weeks"
msgstr "კვირა"

#: ui/DurationPromptDialog.qml:167
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "months"
msgstr "თვე"

#: ui/DurationPromptDialog.qml:169
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "years"
msgstr "წელი"

#: ui/main.qml:23
#, kde-format
msgctxt "@action:button"
msgid "Configure Appearance…"
msgstr "გარეგნობის მორგება…"

#: ui/main.qml:31
#, kde-format
msgctxt "Screen will not lock automatically"
msgid "Never"
msgstr "არასდროს"

#: ui/main.qml:32 ui/main.qml:46
#, kde-format
msgid "1 minute"
msgstr "1 წუთი"

#: ui/main.qml:33
#, kde-format
msgid "2 minutes"
msgstr "2 წუთი"

#: ui/main.qml:34 ui/main.qml:47
#, kde-format
msgid "5 minutes"
msgstr "5 წუთი"

#: ui/main.qml:35
#, kde-format
msgid "10 minutes"
msgstr "10 წუთი"

#: ui/main.qml:36 ui/main.qml:48
#, kde-format
msgid "15 minutes"
msgstr "15 წუთი"

#: ui/main.qml:37
#, kde-format
msgid "30 minutes"
msgstr "30 წუთი"

#: ui/main.qml:38 ui/main.qml:49
#, kde-format
msgctxt "To add a custom value, not in the predefined list"
msgid "Custom"
msgstr "მორგებული"

#: ui/main.qml:43
#, kde-format
msgctxt "The grace period is disabled"
msgid "Require password immediately"
msgstr "პაროლი დაუყოვნებლივი მოთხოვნა"

#: ui/main.qml:44
#, kde-format
msgid "5 seconds"
msgstr "5 წამი"

#: ui/main.qml:45
#, kde-format
msgid "30 seconds"
msgstr "30 წამი"

#: ui/main.qml:59
#, kde-format
msgid "Lock screen automatically:"
msgstr "ეკრანის ავტომატური დაბლოკვა:"

#: ui/main.qml:93 ui/main.qml:179 ui/main.qml:303 ui/main.qml:374
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 წუთი"
msgstr[1] "%1 წუთი"

#: ui/main.qml:109
#, kde-format
msgctxt "@option:check"
msgid "Lock after waking from sleep"
msgstr "დაბლოკვა ძილიდან გაღვიძების შემდეგ"

#: ui/main.qml:126
#, kde-format
msgctxt "First part of sentence \"Delay before password required: X minutes\""
msgid "Delay before password required:"
msgstr "დაყოვნება პაროლის მოთხოვნამდე:"

#: ui/main.qml:180 ui/main.qml:376
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 წამი"
msgstr[1] "%1 წამი"

#: ui/main.qml:202
#, kde-format
msgid "Keyboard shortcut:"
msgstr "კლავიატურის მალსახმობი:"

#: ui/main.qml:272 ui/main.qml:330
#, kde-format
msgctxt "@title:window"
msgid "Custom Duration"
msgstr "ხანგრძლივობის მორგება"

#~ msgctxt "@action:button"
#~ msgid "Confirm"
#~ msgstr "დადასტურება"

#~ msgctxt "@action:button"
#~ msgid "Cancel"
#~ msgstr "გაუქმება"

#~ msgid "Appearance:"
#~ msgstr "გარეგნობა:"

#~ msgctxt "@action:button"
#~ msgid "Configure..."
#~ msgstr "მორგება..."

#~ msgctxt "First part of sentence \"Automatically after X minutes\""
#~ msgid "After"
#~ msgstr "შემდეგ"

#~ msgctxt "@label:spinbox"
#~ msgid "Allow unlocking without password for:"
#~ msgstr "პაროლის გარეშე განბლოკვისთვის:"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Temuri Doghonadze"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "Temuri.doghonadze@gmail.com"
