# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2014, 2015, 2016, 2017, 2018, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-19 00:39+0000\n"
"PO-Revision-Date: 2020-07-02 20:39+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.3\n"

#: ui/Appearance.qml:22
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Utseende"

#: ui/Appearance.qml:38
#, kde-format
msgid "Wallpaper type:"
msgstr "Typ av skrivbordsunderlägg:"

#: ui/DurationPromptDialog.qml:114
#, fuzzy, kde-format
#| msgid "%1 second"
#| msgid_plural "%1 seconds"
msgctxt "The unit of the time input field"
msgid "millisecond"
msgid_plural "milliseconds"
msgstr[0] "%1 sekund"
msgstr[1] "%1 sekunder"

#: ui/DurationPromptDialog.qml:116
#, fuzzy, kde-format
#| msgid "%1 second"
#| msgid_plural "%1 seconds"
msgctxt "The unit of the time input field"
msgid "second"
msgid_plural "seconds"
msgstr[0] "%1 sekund"
msgstr[1] "%1 sekunder"

#: ui/DurationPromptDialog.qml:118
#, fuzzy, kde-format
#| msgid "%1 minute"
#| msgid_plural "%1 minutes"
msgctxt "The unit of the time input field"
msgid "minute"
msgid_plural "minutes"
msgstr[0] "%1 minut"
msgstr[1] "%1 minuter"

#: ui/DurationPromptDialog.qml:120
#, kde-format
msgctxt "The unit of the time input field"
msgid "hour"
msgid_plural "hours"
msgstr[0] ""
msgstr[1] ""

#: ui/DurationPromptDialog.qml:122
#, kde-format
msgctxt "The unit of the time input field"
msgid "day"
msgid_plural "days"
msgstr[0] ""
msgstr[1] ""

#: ui/DurationPromptDialog.qml:124
#, kde-format
msgctxt "The unit of the time input field"
msgid "week"
msgid_plural "weeks"
msgstr[0] ""
msgstr[1] ""

#: ui/DurationPromptDialog.qml:126
#, kde-format
msgctxt "The unit of the time input field"
msgid "month"
msgid_plural "months"
msgstr[0] ""
msgstr[1] ""

#: ui/DurationPromptDialog.qml:128
#, kde-format
msgctxt "The unit of the time input field"
msgid "year"
msgid_plural "years"
msgstr[0] ""
msgstr[1] ""

#: ui/DurationPromptDialog.qml:155
#, fuzzy, kde-format
#| msgid "%1 second"
#| msgid_plural "%1 seconds"
msgctxt "@text:radiobutton Unit of the time input field"
msgid "milliseconds"
msgstr "%1 sekund"

#: ui/DurationPromptDialog.qml:157
#, fuzzy, kde-format
#| msgid "%1 second"
#| msgid_plural "%1 seconds"
msgctxt "@text:radiobutton Unit of the time input field"
msgid "seconds"
msgstr "%1 sekund"

#: ui/DurationPromptDialog.qml:159
#, fuzzy, kde-format
#| msgid "%1 minute"
#| msgid_plural "%1 minutes"
msgctxt "@text:radiobutton Unit of the time input field"
msgid "minutes"
msgstr "%1 minut"

#: ui/DurationPromptDialog.qml:161
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "hours"
msgstr ""

#: ui/DurationPromptDialog.qml:163
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "days"
msgstr ""

#: ui/DurationPromptDialog.qml:165
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "weeks"
msgstr ""

#: ui/DurationPromptDialog.qml:167
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "months"
msgstr ""

#: ui/DurationPromptDialog.qml:169
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "years"
msgstr ""

#: ui/main.qml:23
#, fuzzy, kde-format
#| msgctxt "@title"
#| msgid "Appearance"
msgctxt "@action:button"
msgid "Configure Appearance…"
msgstr "Utseende"

#: ui/main.qml:31
#, fuzzy, kde-format
#| msgid "Never"
msgctxt "Screen will not lock automatically"
msgid "Never"
msgstr "Aldrig"

#: ui/main.qml:32 ui/main.qml:46
#, fuzzy, kde-format
#| msgid "%1 minute"
#| msgid_plural "%1 minutes"
msgid "1 minute"
msgstr "%1 minut"

#: ui/main.qml:33
#, fuzzy, kde-format
#| msgid "%1 minute"
#| msgid_plural "%1 minutes"
msgid "2 minutes"
msgstr "%1 minut"

#: ui/main.qml:34 ui/main.qml:47
#, fuzzy, kde-format
#| msgid "%1 minute"
#| msgid_plural "%1 minutes"
msgid "5 minutes"
msgstr "%1 minut"

#: ui/main.qml:35
#, fuzzy, kde-format
#| msgid "%1 minute"
#| msgid_plural "%1 minutes"
msgid "10 minutes"
msgstr "%1 minut"

#: ui/main.qml:36 ui/main.qml:48
#, fuzzy, kde-format
#| msgid "%1 minute"
#| msgid_plural "%1 minutes"
msgid "15 minutes"
msgstr "%1 minut"

#: ui/main.qml:37
#, fuzzy, kde-format
#| msgid "%1 minute"
#| msgid_plural "%1 minutes"
msgid "30 minutes"
msgstr "%1 minut"

#: ui/main.qml:38 ui/main.qml:49
#, kde-format
msgctxt "To add a custom value, not in the predefined list"
msgid "Custom"
msgstr ""

#: ui/main.qml:43
#, fuzzy, kde-format
#| msgid "Re&quire password after locking:"
msgctxt "The grace period is disabled"
msgid "Require password immediately"
msgstr "&Kräv lösenord efter låsning:"

#: ui/main.qml:44
#, fuzzy, kde-format
#| msgid "%1 second"
#| msgid_plural "%1 seconds"
msgid "5 seconds"
msgstr "%1 sekund"

#: ui/main.qml:45
#, fuzzy, kde-format
#| msgid "%1 second"
#| msgid_plural "%1 seconds"
msgid "30 seconds"
msgstr "%1 sekund"

#: ui/main.qml:59
#, kde-format
msgid "Lock screen automatically:"
msgstr "Lås skärmen automatiskt:"

#: ui/main.qml:93 ui/main.qml:179 ui/main.qml:303 ui/main.qml:374
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minut"
msgstr[1] "%1 minuter"

#: ui/main.qml:109
#, fuzzy, kde-format
#| msgctxt "@option:check"
#| msgid "After waking from sleep"
msgctxt "@option:check"
msgid "Lock after waking from sleep"
msgstr "Vid uppvaknande ur viloläge"

#: ui/main.qml:126
#, kde-format
msgctxt "First part of sentence \"Delay before password required: X minutes\""
msgid "Delay before password required:"
msgstr ""

#: ui/main.qml:180 ui/main.qml:376
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 sekund"
msgstr[1] "%1 sekunder"

#: ui/main.qml:202
#, kde-format
msgid "Keyboard shortcut:"
msgstr "Snabbtangent:"

#: ui/main.qml:272 ui/main.qml:330
#, fuzzy, kde-format
#| msgid "Custom Background:"
msgctxt "@title:window"
msgid "Custom Duration"
msgstr "Egen bakgrund:"

#, fuzzy
#~| msgctxt "@action:button"
#~| msgid "Configure..."
#~ msgctxt "@action:button"
#~ msgid "Confirm"
#~ msgstr "Anpassa..."

#~ msgid "Appearance:"
#~ msgstr "Utseende:"

#~ msgctxt "@action:button"
#~ msgid "Configure..."
#~ msgstr "Anpassa..."

#~ msgctxt "First part of sentence \"Automatically after X minutes\""
#~ msgid "After"
#~ msgstr "Efter"

#~ msgctxt "@label:spinbox"
#~ msgid "Allow unlocking without password for:"
#~ msgstr "Tillåt upplåsning utan lösenord för:"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Stefan Asserhäll"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "stefan.asserhall@bredband.net"

#~ msgid "Screen Locking"
#~ msgstr "Skärmlåsning"

#~ msgid "Martin Gräßlin"
#~ msgstr "Martin Gräßlin"

#~ msgid "Kevin Ottens"
#~ msgstr "Kevin Ottens"

#~ msgid "Activation"
#~ msgstr "Aktivering"

#~ msgid "Automatically after:"
#~ msgstr "Automatiskt efter:"

#~ msgid "Lock screen when waking up from suspension"
#~ msgstr "Låser skärmen vid uppvaknande ur viloläge"

#~ msgid "Immediately"
#~ msgstr "Genast"

#~ msgid "The global keyboard shortcut to lock the screen."
#~ msgstr "Den globala genvägen för att låsa skärmen."

#~ msgid "Error"
#~ msgstr "Fel"

#~ msgid "Failed to successfully test the screen locker."
#~ msgstr "Misslyckades testa skärmlåsning med lyckat resultat."

#~ msgid "Lock Session"
#~ msgstr "Lås session"

#~ msgid "&Lock screen on resume:"
#~ msgstr "&Lås skärmen vid återgång:"

#~ msgid "Wallpaper"
#~ msgstr "Skrivbordsunderlägg"

#~ msgid "Load from file..."
#~ msgstr "Läs in från fil..."

#~ msgid "Clear Image"
#~ msgstr "Rensa bild"

#~ msgid "Select image"
#~ msgstr "Välj bild"

#~ msgid "..."
#~ msgstr "..."

#~ msgid "Form"
#~ msgstr "Formulär"
